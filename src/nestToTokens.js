//原本的数组是只有一层,这个方法实现数组的折叠
export default function nestToTokens(tokens){
  //定义最终折叠数组
  const nestedTokens = []
  //这是一个栈结构,就想想羽毛球筒,就是一个工具,让我先入后出,完全适配这种折叠数组的逻辑,所以用它
  //栈啥用没有,就是记录层级的,记录结构的.模板引擎的双层遍历的那种结构
  //sections的存在是为了明晰结构,给收集器提供指向的目标
  const sections = []
  //这是一个收集器,先天就等于最终数组,只要遇到#,就会改变指向,指向#那一项的下标为2的位置(该位置我们会让他是数组)
  let collector = nestedTokens
  for(var i=0;i<tokens.length;i++){
    let token = tokens[i]
    // 判断双层数组的每一项的首项是不是#or/
    // 下面的switch你注意看我的序号分析,按1234顺序执行
    switch(token[0]){
      case '#':
        // 2.这里第一次遇到#号,收集器直接把#号项收集(此时收集器不是空的,经历1,收集器收集了开头的那些没#的项目)
        collector.push(token)
        //将#号项压入栈,如果是第一次压入,栈只有一项
        sections.push(token)
        // 3.改变收集器指向,指向#号项的下标为2的位置
        collector = token[2] = []
        break;
      case '/':
        // 遇到/就出栈,第一次出栈,记录一下,最后(最里面)进来的先弹出,删除
        sections.pop()
        //5.出栈后要改变收集器指向,判断栈内是否有#项了,没有,收集器就指向最终nestedTokens,有的话就指向余下的最后的#项的下标为2的项(数组)
        collector = sections.length > 0 ? sections[sections.length-1][2]:nestedTokens
        break;
      default:
        // 1.首先执行这个,这个时候还没有遇到#项,这时候收集器就是最终nestedTokens,等到第一次遇到#请看2
        // 4.经过3,然后再遇到没#的项目,都会被push入收集器,这时候收集器是指向#项下标为2的那个空数组的
        // 6.最终#都搞完了,栈结构也全部弹出,这时候收集器指向nestedTokens,这时候继续往nestedTokens里面塞一些最外层的项
        collector.push(token)
        // zzz.以上,表面上是nestedTokens内塞入东西,真实情况是,收集器最开始指向nestedTokens,它伪装成nestedTokens,并且收集了一些#的壳子,
        // yyy.然后收集器开始变形,变成了#项,的第三个空数组参数,又往里面收集东西
        // xxx.最终,很显然,东西都在nestedTokens里面,掌握了壳子,就掌握了一切,你收集器往里面塞东西把
    }
  }
  return nestedTokens
}