export class Scanner {
  constructor(templateStr){
    this.templateStr = templateStr
    this.pos = 0//指针
    this.tail = templateStr//尾巴
  }
  scan(tag){
    if(this.tail.indexOf(tag) == 0){
      this.pos+=tag.length
      this.tail = this.templateStr.substring(this.pos)
    }
  }
  scanUtil(stopTag){
    const pos_backup = this.pos
    //传入的字符串{{不在尾巴的第一位,也就是指针还没移动到{上,并且指针还不能移动到最后
    while(!this.eos() && this.tail.indexOf(stopTag) != 0){
      this.pos++
      this.tail = this.templateStr.substring(this.pos)
    }
    return this.templateStr.substring(pos_backup,this.pos)
  }
  // 判断指针有没有到头
  eos(){
    return this.pos >= this.templateStr.length
  }
}