import {Scanner} from './Scanner.js'
import nestToTokens from './nestToTokens.js'
export default function parseTemplateToTokens(templateStr){
  const scanner = new Scanner(templateStr)
  const tokens = []
  var word
  while(!scanner.eos()){
    word = scanner.scanUtil('{{')
    tokens.push(['text',word])
    scanner.scan('{{')
    word = scanner.scanUtil('}}')
    scanner.scan('}}')
    if(word != ''){
      if(word[0] === '#'){
        tokens.push(['#',word.slice(1)])
      } else if(word[0] === '/'){
        tokens.push(['/',word.slice(1)])
      } else {
        tokens.push(['text',word])
      }
    }
  }
  return nestToTokens(tokens)
}